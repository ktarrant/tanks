package com.ktarrant.tanks.terrain;

public enum NeighborMap {
	TOP_LEFT 	(0, -1, -1),
	TOP_MID		(1,  0, -1),
	TOP_RIGHT	(2,  1, -1),
	MID_LEFT 	(3, -1,  0),
	MID_MID		(4,  0,  0),
	MID_RIGHT	(5,  1,  0),
	BOT_LEFT 	(6, -1,  1),
	BOT_MID		(7,  0,  1),
	BOT_RIGHT	(8,  1,  1);
	
	private final int index;
	private final int xOffset;
	private final int yOffset;
	
	private NeighborMap(int index, int xOffset, int yOffset) {
		this.index = index;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	
	public int getSubCellXOffset() {
		return xOffset + 1;
	}
	
	public int getSubCellYOffset() {
		return yOffset + 1;
	}
	
	public int getNeighborXOffset() {
		return xOffset;
	}
	
	public int getNeighborYOffset() {
		return yOffset;
	}
	
	public int getIndex() {	
		return index;
	}
	
	public static int count() {
		return values().length;
	}
	
	public static int getCellLength() {
		return 3;
	}
}
