package com.ktarrant.tanks.terrain;

import static org.junit.Assert.*;

import org.junit.Test;

import com.badlogic.gdx.maps.tiled.TiledMap;

public class TerrainLayerTest {
	public static final int TEST_MAP_WIDTH = 16;
	public static final int TEST_MAP_HEIGHT = 16;
	public static final int TEST_MAP_TILE_WIDTH = 128;
	public static final int TEST_MAP_TILE_HEIGHT = 128;
	

	@Test
	public void test() {
		TiledMap map = new TiledMap();
		TiledMapTerrainLayer terrainLayer = 
				new TiledMapTerrainLayer(TEST_MAP_WIDTH, TEST_MAP_HEIGHT,
						TEST_MAP_TILE_WIDTH, TEST_MAP_TILE_HEIGHT);
		map.getLayers().add(terrainLayer);
		
	}

}
