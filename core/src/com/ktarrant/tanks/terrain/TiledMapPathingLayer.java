package com.ktarrant.tanks.terrain;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class TiledMapPathingLayer extends TiledMapTileLayer {

	public TiledMapPathingLayer(int width, int height, int tileWidth,
			int tileHeight) {
		super(width, height, tileWidth, tileHeight);
		
		
	}
	
	/**
	 * Returns whether the cell at x,y is walkable or not. If x,y out of bounds,
	 * returns false.
	 * @param x The X-coordinate
	 * @param y The Y-coordinate
	 * @return the isWalkable true if the cell can be walked on, false if
	 * the cell cannot be walked on. 
	 */
	public boolean isWalkable(int x, int y) {
		Cell cell = (Cell) getCell(x, y);
		if (cell == null) {
			return false;
		} else {
			return cell.isWalkable();
		}
	}

	/**
	 * Sets whether the cell at x,y is walkable or not.
	 * @param x The X-coordinate
	 * @param y The Y-coordinate
	 * @param isWalkable true if the cell can be walked on, false if the
	 * cell cannot be walked on.
	 */
	public void setWalkable(int x, int y, boolean isWalkable) {
		Cell cell = (Cell) getCell(x, y);
		if (cell != null) {
			cell.setWalkable(false);
		}
	}
	
	public static class Cell extends TiledMapTileLayer.Cell {
		private boolean isWalkable;
		
		public Cell() {
			super();
			
			this.isWalkable = false;
		}

		/**
		 * @return the isWalkable true if the cell can be walked on, false if
		 * the cell cannot be walked on.
		 */
		public boolean isWalkable() {
			return isWalkable;
		}

		/**
		 * @param isWalkable true if the cell can be walked on, false if the
		 * cell cannot be walked on.
		 */
		public void setWalkable(boolean isWalkable) {
			this.isWalkable = isWalkable;
		}
	}
}
