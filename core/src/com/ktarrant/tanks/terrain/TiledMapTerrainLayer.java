package com.ktarrant.tanks.terrain;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class TiledMapTerrainLayer extends TiledMapPathingLayer {
	private int gameMapWidth;
	private int gameMapHeight;
	private TerrainCell[][] terrainCells;
	
	/**
	 * Initializes the TerrainTileLayer with a gameMapWidth by gameMapHeight
	 * grid of Cells.
	 * @param gameMapWidth The width of the TerrainTileLayer in Cells. The
	 * "true" width of the map is three times this value, because of every
	 * GameTile contains a 3x3 grid of SubCells.
	 * @param gameMapHeight The height of the TerrainTileLayer in Cells. The
	 * "true" height of the map is three times this value, because of every
	 * GameTile contains a 3x3 grid of SubCells.
	 * @param tileWidth The width in pixels of every SubTile.
	 * @param tileHeight The height in pixels of every SubTile.
	 */
	public TiledMapTerrainLayer(int gameMapWidth, int gameMapHeight,
			int tileWidth, int tileHeight) {
		super(gameMapWidth * 3, gameMapHeight * 3, tileWidth, tileHeight);
		
		this.gameMapWidth = gameMapWidth;
		this.gameMapHeight = gameMapHeight;
		
		terrainCells = new TerrainCell[gameMapWidth][gameMapHeight];
	}

	/**
	 * @return The width of the TerrainTileLayer in Cells. The "true" width
	 * of the map is three times this value, because of every GameTile contains
	 * a 3x3 grid of SubCells.
	 */
	public int getGameMapWidth() {
		return gameMapWidth;
	}
	
	/**
	 * @return The height of the TerrainTileLayer in Cells. The "true"
	 * height of the map is three times this value, because of every GameTile
	 * contains a 3x3 grid of SubCells.
	 */
	public int getGameMapHeight() {
		return gameMapHeight;
	}
	
	/**
	 * Returns the TerrainCell at the given x, y.
	 * @param x The x-position of the TerrainCell to get.
	 * @param y The y-position of the TerrainCell to get.
	 * @return The TerrainCell at position x,y or null if x,y is out of bounds
	 * or no TerrainCell is found there.
	 */
	public TerrainCell getTerrainCell(int x, int y) {
		if (x < 0 || x >= getGameMapWidth() || y < 0 || y >= getGameMapHeight()){
			return null;
		} else {
			return terrainCells[x][y];
		}
	}
	
	/**
	 * Sets the TerrainType of the TerrainCell at the given x, y. Updates the
	 * TerrainCell x,y value to match the cell's place in the layer.
	 * @param x The x-position of the TerrainCell to set.
	 * @param y The y-position of the TerrainCell to set.
	 * @param z The z-position to assign to the TerrainCell
	 * @param terrainType return The TerrainType to assign to the TerrainCell at
	 * position x,y.
	 *
	 */
	public void setTerrainCell(int x, int y, TerrainCell cell) {
		if (x >= 0 && x < getGameMapWidth() && 
			y >= 0 && y < getGameMapHeight()) {
			if (cell != null) {
				cell.setX(x);
				cell.setY(y);
			}
			terrainCells[x][y] = cell;
		}
	}
	
	public class TerrainCell {
		private int x;
		private int y;
		private int z;
		private TerrainType terrainType;
		
		/**
		 * Creates the TerrainTileLayer.Cell object.
		 * @param x The x-position, in Cells, of the Cell to be generated. Will
		 * be generated from the SubCells in that location.
		 * @param y The y-position, in Cells, of the Cell to be generated. Will
		 * be generated from the SubCells in that location.
		 * @param z The z-position, representing the "plateau level" of the
		 * Cell.
		 * @param terrainType The type of terrain this cell contains.
		 */
		public TerrainCell(int x, int y, int z, TerrainType terrainType) {
			setX(x);
			setY(y);
			setZ(z);
			setTerrainType(terrainType);
		}
		
		/**
		 * Creates the TerrainTileLayer.Cell object
		 * @param z The z-position, representing the "plateau level" of the
		 * Cell.
		 * @param terrainType The type of terrain this cell contains.
		 */
		public TerrainCell(int z, TerrainType terrainType) {
			this(0, 0, z, terrainType);
		}
		
		/**
		 * Creates the TerrainTileLayer.Cell object
		 */
		public TerrainCell() {
			this(0, 0, 0, null);
		}
		
		/** Update the SubCells controlled by this Cell, using knowledge of the
		 *  neighboring Cells and the TerrainTileSet provided by the parent
		 *  TerrainTileLayer.
		 */
		public void update() {
			//Fill the Neighbor array with the TerrainCell neighbors
			int subLen = NeighborMap.getCellLength();
			for (NeighborMap mapping : NeighborMap.values()) {
				int nbrX = x + mapping.getNeighborXOffset();
				int nbrY = y + mapping.getNeighborYOffset();
				int subX = subLen * x + mapping.getSubCellXOffset();
				int subY = subLen * y + mapping.getSubCellYOffset();
				TerrainCell neighbor = getTerrainCell(nbrX, nbrY);
				Cell subCell = (Cell) getCell(subX, subY);
				if (neighbor != null) {
					//Update the subcell based on the neighbor
				}
			}
		}

		/**
		 * @return The x-position of this TerrainCell in the layer.
		 */
		public int getX() {
			return x;
		}
		
		/**
		 * @param x The x-value of the TerrainCell to set.
		 */
		public void setX(int x) {
			this.x = z;
		}

		/**
		 * @return The y-position of this TerrainCell in the layer.
		 */
		public int getY() {
			return y;
		}
		
		/**
		 * @param y The y-value of the TerrainCell to set.
		 */
		public void setY(int y) {
			this.y = y;
		}

		/**
		 * @return The z-position of this TerrainCell.
		 */
		public int getZ() {
			return z;
		}

		/**
		 * @param z The z-value of the TerrainCell to set.
		 */
		public void setZ(int z) {
			this.z = z;
		}

		/**
		 * @return The terrainType of this TerrainCell.
		 */
		public TerrainType getTerrainType() {
			return terrainType;
		}

		/**
		 * @param terrainType The terrainType of the TerrainCell to set.
		 */
		public void setTerrainType(TerrainType terrainType) {
			this.terrainType = terrainType;
		}
	}
}
